
<nav>
    <div class="nav-wrapper">
        <a href="#" class="brand-logo center"><span style="color: white ; font-family: 'Dancing Script'">French</span> <span style="color:white ; font-family: 'Pacifico' ">Chic</span></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">

        </ul>
    </div>
</nav>
<div class="container">
    <h4 style="color: gray ; font-family: 'Dancing Script'">Votre Panier</h4>

    <table class="striped">
        <tr>
            <th>Libell�</th>
            <th>Prix</th>
            <th>Quantit�</th>
            <th>Montant</th>
            <th>Stock</th>
        </tr>


            <tr>
            <td>${reponseTraiterAjoutPanier.laCommande.ligneCommande.produit.libelle}</td>
             <td>${reponseTraiterAjoutPanier.laCommande.ligneCommande.produit.prix}</td>
             <td>${reponseTraiterAjoutPanier.laCommande.ligneCommande.quantite}</td>
             <td>${reponseTraiterAjoutPanier.laCommande.ligneCommande.montant}</td>
             <td>${reponseTraiterAjoutPanier.laCommande.ligneCommande.produit.quantiteEnStock}</td>
        </tr>
    </table>
        <br>
        <h5  style="color:#ef9a9a ; font-family: 'Pacifico' ">Montant Panier : <b>${reponseTraiterAjoutPanier.laCommande.montant}</b></h5>
</div>


