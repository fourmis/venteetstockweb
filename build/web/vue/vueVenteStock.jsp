<%-- 
    Document   : vue_vente_stock
    Created on : May 23, 2021, 11:39:33 AM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="controlleur.EnumTypeEcran" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/login.css">
        <link rel="stylesheet" href="css/materialize.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
       <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script&family=Fredoka+One&family=Oi&family=Pacifico&family=Roboto:ital,wght@0,100;0,300;1,100&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=KoHo:ital,wght@1,200&family=Ubuntu:ital,wght@1,300&display=swap" rel="stylesheet">        
<title>JSP Page</title>
    </head>
    <body>
    
          <c:choose>
            <c:when test="${ecran_acceuil == 'ECRAN_ACCUEIL'}">
                 <jsp:include page="../partial/ecran_accueil.jsp" />
            </c:when>
                
            <c:when test="${reponseTraiterIdentification != null}" >
                
                <c:if test="${reponseTraiterIdentification.enumTypeEcran == 'ECRAN_PERSO'}">
                      <jsp:include page="../partial/ecran_accueil_personnalise.jsp" />
                </c:if>
              
                
            </c:when>
            
              <c:when test="${reponseTraiterAjoutPanier != null}" >
                  <c:if test="${reponseTraiterAjoutPanier.typeEcran == 'ECRAN_PANIER'}">
                       <jsp:include page="../partial/ecran_panier.jsp" />
                  </c:if>
               
             </c:when>
            
            <c:otherwise>
                 <jsp:forward page="../Session"></jsp:forward>
            </c:otherwise>
        </c:choose>
       
    
      
   
    
    <script src="js/materialize.min.js"></script>
    </body>
</html>
