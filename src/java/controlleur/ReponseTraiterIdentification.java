/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlleur;

import metier.Client;
import metier.Produit;

/**
 *
 * @author Dell
 */
public class ReponseTraiterIdentification {
        private EnumTypeEcran enumTypeEcran;
    private Client client;
    private Produit produit;

    public ReponseTraiterIdentification(EnumTypeEcran enumTypeEcran, Client client, Produit produit) {
        this.enumTypeEcran = enumTypeEcran;
        this.client = client;
        this.produit = produit;
    }

    public EnumTypeEcran getEnumTypeEcran() {
        return enumTypeEcran;
    }

    public void setEnumTypeEcran(EnumTypeEcran enumTypeEcran) {
        this.enumTypeEcran = enumTypeEcran;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }
}
