/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlleur;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import metier.Client;
import metier.Commande;
import metier.LigneCommande;
import metier.Produit;

/**
 *
 * @author Dell
 */
public class Session extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forwardTo = "";
        request.setAttribute("ecran_acceuil", traiterConnexion());
        forwardTo = "vue/vueVenteStock.jsp";
        RequestDispatcher dp = request.getRequestDispatcher(forwardTo);
        dp.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String forwardTo = "";
        String action = request.getParameter("action");

        switch (action) {
            case "traiterIdentification": {
                String pseudo = request.getParameter("pseudo");
                String mot_de_passe = request.getParameter("mot_de_passe");
                ReponseTraiterIdentification reponseTraiterIdentification = traiterIdentification(pseudo, mot_de_passe);
                request.setAttribute("reponseTraiterIdentification", reponseTraiterIdentification);
                forwardTo = "vue/vueVenteStock.jsp";
                RequestDispatcher dp = request.getRequestDispatcher(forwardTo);
                dp.forward(request, response);
                break;
            }

            case "traiterAjoutPanier": {
                int qte = Integer.parseInt(request.getParameter("quantite"));
                Produit produit = Produit.rechercherProduitDuJour();
                TraiterAjoutPanierReponse reponseTraiterAjoutPanier = traiterAjoutPanier(produit, qte);
                request.setAttribute("reponseTraiterAjoutPanier", reponseTraiterAjoutPanier);
                
              
                
                forwardTo = "vue/vueVenteStock.jsp";
                RequestDispatcher dp = request.getRequestDispatcher(forwardTo);
                dp.forward(request, response);
                
                
                break;
            }

        }

    }
    
    public EnumTypeEcran traiterConnexion() {
      
        return EnumTypeEcran.ECRAN_ACCUEIL;
    }

    public ReponseTraiterIdentification traiterIdentification(String pseudo, String motDePasse) {
        Client client = Client.rechercherClient(pseudo, motDePasse);
        Produit produit = Produit.rechercherProduitDuJour();
        return new ReponseTraiterIdentification(EnumTypeEcran.ECRAN_PERSO, client, produit);
    }

    public TraiterAjoutPanierReponse traiterAjoutPanier(Produit produit, int intg) {
        TraiterAjoutPanierReponse traiterAjoutPanierReponse = new TraiterAjoutPanierReponse();
        Commande commande = new Commande();
        commande.ajouterProduit(produit, intg);
        traiterAjoutPanierReponse.setTypeEcran(EnumTypeEcran.ECRAN_PANIER); 
        traiterAjoutPanierReponse.setLaCommande(commande); 
        return traiterAjoutPanierReponse;
    }
    
   
    

}
