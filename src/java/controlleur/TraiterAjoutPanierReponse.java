/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlleur;

import metier.Commande;

/**
 *
 * @author Dell
 */
public class TraiterAjoutPanierReponse {
    private EnumTypeEcran  typeEcran ;
    private Commande laCommande;

    public EnumTypeEcran getTypeEcran() {
        return typeEcran;
    }

    public void setTypeEcran(EnumTypeEcran typeEcran) {
        this.typeEcran = typeEcran;
    }

    public Commande getLaCommande() {
        return laCommande;
    }

    public void setLaCommande(Commande laCommande) {
        this.laCommande = laCommande;
    }
    
    
    
}
