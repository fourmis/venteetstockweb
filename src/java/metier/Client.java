/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.util.ArrayList;

/**
 *
 * @author Dell
 */
public class Client {
   
    public static ArrayList<Client> lesClients = new ArrayList(){{
        add(new Client("001", "Marie", "Dupond", "mdupond", "1234"));
    }};

    private String numero;
    private String nom;
    private String prenom;
    private String pseudo;
    private String motDePasse;

    public static  Client rechercherClient(String pseudo, String motDePasse){
        Client client = null;
        for (Client clientTemp: lesClients) {
            if(clientTemp.getPseudo().equals(pseudo) && clientTemp.getMotDePasse().equals(motDePasse)){
                client= clientTemp;
                break;
            }
        }
        return client;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Client(String numero, String nom, String prenom, String pseudo, String motDePasse) {
        this.numero = numero;
        this.nom = nom;
        this.prenom = prenom;
        this.pseudo = pseudo;
        this.motDePasse = motDePasse;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", pseudo='" + pseudo + '\'' +
                ", motDePasse='" + motDePasse + '\'' +
                '}';
    }
}
