/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.util.ArrayList;

/**
 *
 * @author Dell
 */
public class Commande {

    public static ArrayList<Commande> lesCommande = new ArrayList();

    private String numero ;
    private float montant ;
    private LigneCommande ligneCommande;

    public Commande() {
        this.numero = "CM"+lesCommande.size()+1;
        this.montant = 0 ;
    }

    public void ajouterProduit(Produit leProduit, int quantite ){
        LigneCommande  ligneCommande = new LigneCommande();
        ligneCommande.setQuantite(quantite);
        ligneCommande.setMontant(leProduit.getPrix()*quantite);
        leProduit.retirerDuStock(quantite);
        ligneCommande.setProduit(leProduit);
     
        this.ligneCommande=ligneCommande;
        this.montant= this.montant+ligneCommande.getMontant();
    }


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) {
        this.montant = montant;
    }

    public static ArrayList<Commande> getLesCommande() {
        return lesCommande;
    }

    public static void setLesCommande(ArrayList<Commande> lesCommande) {
        Commande.lesCommande = lesCommande;
    }

    public LigneCommande getLigneCommande() {
        return ligneCommande;
    }

    public void setLigneCommande(LigneCommande ligneCommande) {
        this.ligneCommande = ligneCommande;
    }


    
    
}
    

